<?php


class Database {

  private $host = 'mariadb';
  private $db = 'php';
  private $user = 'root';
  private $pass = 'password';
  private $port = '3306';
  public $dbConn = null;

  public function getConnection() {
    try {
      $dsn = "mysql:dbname=$this->db;host=$this->host;port=$this->port;charset=utf8";
      $this->dbConn = new \PDO($dsn, $this->user, $this->pass);
    } catch (PDOException $exception) {
      echo 'Connection error: ' . $exception->getMessage();
    }

    return $this->dbConn;
  }

}